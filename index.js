const puppeteer = require('puppeteer');
const fs = require('fs');
const fornamn = 'XXXX';
const efternamn = 'XXXXX'
const email = 'XXXX@XXXXXX.XX'
const phone = '123456789'

const youShallPass = async (cnt) => {
  setTimeout(async () => {
    logStep('Trying to make a booking, try: ' + cnt, true);
    const result = await tryToMakeBooking();
    if(!result){
      youShallPass(cnt + 1);
    }
  }, 2000);
}

const tryToMakeBooking = async () => {

  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage()
  const navigationPromise = page.waitForNavigation({ timeout: 100000 })
  let currentStep = 'Initial';
  try{

  currentStep = logStep('Navigating to main page');
  await page.goto('https://bokapass.nemoq.se/Booking/Booking/Index/vastragotaland', { timeout: 100000})

  await page.setViewport({ width: 1280, height: 1281 })

  await page.waitForSelector('.span6:nth-child(1) > .Box > .form-horizontal > .control-group > .btn', {timeout: 100000})
  await page.click('.span6:nth-child(1) > .Box > .form-horizontal > .control-group > .btn')
  currentStep = logStep('Clicked first button');
  await navigationPromise
  currentStep = logStep('Navigating to accept page');
  await page.waitForSelector('#AcceptInformationStorage', {timeout: 100000})
  currentStep = logStep('Found accept box');
  await page.click('#AcceptInformationStorage')

  await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary', {timeout: 100000})
  currentStep = logStep('Found accept button');
  await page.click('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Accept button clicked');
  await navigationPromise
  currentStep = logStep('Navigating to county page');
  await page.waitForSelector('#ServiceCategoryCustomers_0__ServiceCategoryId', {timeout: 100000})
  currentStep = logStep('Found country radio-button');
  await page.click('#ServiceCategoryCustomers_0__ServiceCategoryId')

  await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary', {timeout: 100000})
  currentStep = logStep('Found country accept button');
  await page.click('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Clicked country accept button');
  await navigationPromise
  currentStep = logStep('Navigating to select location');
  await page.waitForSelector('#SectionId', {timeout: 100000})
  currentStep = logStep('Found location dropdown');
  await page.click('#SectionId')

  await page.select('#SectionId', '18')
  currentStep = logStep('Borås selected');


  await page.waitForSelector('.form-horizontal > .Box > .control-group > .controls > .btn:nth-child(2)', {timeout: 100000})
  const button = await page.$('.form-horizontal > .Box > .control-group > .controls > .btn:nth-child(2)');
  await button.evaluate(b => b.click());
  currentStep = logStep('Clicked search first avalible button');

  await navigationPromise

  currentStep = logStep('First avalible date loading');

  await page.waitForSelector('#datepicker', {timeout: 100000})
  const date = await page.$eval("#datepicker", (input) => {
    return input.getAttribute("value")
  });
  currentStep = logStep('Found avalible on ' + date);
  const targetStr = '2022-03-30';
  var avalibleDate = new Date(date);
  var targetDate = new Date(targetStr);
  console.log(avalibleDate, 'avalibleDate');
  console.log(targetDate, 'targetDate');
  if(avalibleDate > targetDate) {
    currentStep = logStep('Cant accept date: ' + date + " taget date was: " + targetStr, true);
    await delay(2000);
    await browser.close()
    return false;
  }

  for (var i = 2; i < 7; i += 1){
    const bookingIndex = await findBooking(page, String(i));
    if(bookingIndex != -1) {
      currentStep = logStep('FOUND BOOKING! Child: ' + i + ' item:' + bookingIndex);
      await page.click('tbody > tr > .timetable-cells:nth-child(' + i + ') > .cellcontainer > .pointer:nth-child(' + (bookingIndex + 1) +')');
      currentStep = logStep('Clicked first booking');
      break;
    }
  }

  await page.click('#booking-next')
  currentStep = logStep('Clicked confirm date of booking');
  await navigationPromise
  currentStep = logStep('Checking if booking is avalible');
  await page.waitForSelector('#Customers_0__BookingFieldValues_0__Value', {timeout: 100000})
  await page.$eval('#Customers_0__BookingFieldValues_0__Value', el => el.value = fornamn);
  currentStep = logStep('Entered first name');
  await page.waitForSelector('#Customers_0__BookingFieldValues_1__Value')
  await page.$eval('#Customers_0__BookingFieldValues_1__Value', el => el.value = efternamn);
  currentStep = logStep('Entered last name');
  await page.waitForSelector('#Customers_0__Services_0__IsSelected')
  await page.click('#Customers_0__Services_0__IsSelected')
  currentStep = logStep('Selected pass-checkbox');

  await delay(2000);

  await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Found confirm name button');
  await page.click('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Clicked confirm name button');
  await navigationPromise
  currentStep = logStep('Navigating to info-text page');
  await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary', {timeout: 100000})
  currentStep = logStep('Found confirm info button');
  await page.click('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Clicked confirm info button');
  await navigationPromise

  // KONTAKTUPPGIFTER!!!
  currentStep = logStep('Navigating to contact page');
  await page.waitForSelector('#EmailAddress', {timeout: 100000})
  await page.$eval('#EmailAddress', el => el.value = email);
  currentStep = logStep('Entered email');
  await page.waitForSelector('#ConfirmEmailAddress')
  await page.$eval('#ConfirmEmailAddress', el => el.value = email);
  currentStep = logStep('Entered email again');
  await page.waitForSelector('#PhoneNumber')
  await page.$eval('#PhoneNumber', el => el.value = phone);
  currentStep = logStep('Entered phone');
  await page.waitForSelector('#ConfirmPhoneNumber')
  await page.$eval('#ConfirmPhoneNumber', el => el.value = phone);
  currentStep = logStep('Entered phone again');
  await page.waitForSelector('#SelectedContacts_1__IsSelected')
  await page.click('#SelectedContacts_1__IsSelected')
  currentStep = logStep('Select sms 1');
  await page.waitForSelector('#SelectedContacts_3__IsSelected')
  await page.click('#SelectedContacts_3__IsSelected')
  currentStep = logStep('Select sms 2');

  await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Found button to confirm contact info');
  await page.click('#ContentColumn > #Main > .form-horizontal > .btn-toolbar > .btn-primary')
  currentStep = logStep('Clicked button to confirm contact info');

  await navigationPromise
  currentStep = logStep('Navigating to order summary');
  await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .btn-toolbar:nth-child(1) > .btn')
  currentStep = logStep('Found confirm order button');
  await page.click('#ContentColumn > #Main > .form-horizontal > .btn-toolbar:nth-child(1) > .btn')
  currentStep = logStep('Clicked confirm order button');
  await navigationPromise
  currentStep = logStep('Navigating to order confimation');
  let bookingNr ='Not set';
  try{
    await page.waitForSelector('#ContentColumn > #Main > .form-horizontal > .Box > .control-group:nth-child(2) > .controls > .control-freetext')
    currentStep = logStep('Found booking nr label');
    bookingNr = await page.$eval("#ContentColumn > #Main > .form-horizontal > .Box > .control-group:nth-child(2) > .controls > .control-freetext", (input) => {
      return input.getAttribute("value")
    });
    currentStep = logStep('Read booking number: ' + bookingNr);

    currentStep = logStep('Booking done on: + ' + date + ' with bookingNr: '+ bookingNr, true);
    await delay(5000);
    await page.screenshot({ path: `booking.jpeg` });
  } catch (e) {
    await browser.close()
    // Have to return true if something is going wrong after confirm click, can't risk having multiple bookings!
    currentStep = logStep('Booking MAYBE done on: + ' + date + ' with bookingNr: '+ bookingNr + ' - Error after booking done: ' + e, true);
    return true;
  }

  await delay(30000);
  await browser.close()

  return true;
  } catch (e) {
    currentStep = logStep('Error after: ' + currentStep + ' - ' + e, true);
    await browser.close()
    return false;
  }
}

const delay = (time) => {
  return new Promise(function(resolve) { 
      setTimeout(resolve, time)
  });
}

const logStep = (text, save) => {
  console.log(text);
  if(save){
    var stream = fs.createWriteStream("pass.log", {'flags': 'a'});
    stream.once('open', function(fd) {
      stream.write((new Date().toLocaleString()) + ' - ' + text +"\r\n");
    });
  }
  return text;
}

const findBooking = async (page, child) => {
  const elements = await page.$$('tr > .timetable-cells:nth-child(' + child + ') > .cellcontainer > .pointer');
  console.log(elements.length, 'Number of elements')

  const bookings = await page.$$eval('tr > .timetable-cells:nth-child(' + child + ') > .cellcontainer > .pointer', boxes => {
    return boxes.map(box => box.getAttribute("aria-label"));
   })

  console.log(bookings, 'bookings', child);
  const firstIndex = bookings.findIndex(x => x != 'Bokad');
  return firstIndex;
}

youShallPass(1);